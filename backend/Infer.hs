{-# LANGUAGE TupleSections #-}

{- type inference -}

module Infer where

import Pos
import Syntax
import SyntaxHelpers
import Constraint
import TyDefs
import Solver
import ScopeMap
import qualified Data.Map as M
import Control.Monad
import Control.Monad.State.Lazy
import SubstTy
import Data.List
import Data.Ord
import Data.Maybe (catMaybes)
import Variances
import Renaming
import Imports

{- map a position binding a term variable x to (params,ty), representing
  that x has type forall(params).ty -}
type TmCtxt = M.Map Pos ([TyParamTm],Ty) 
                                      
{- we will map declared type constants c to one of the options here.
   TyDefined gives the body of a type definition; TyDatatype is if c
   is a datatype. -}
data TyInfo = TyDatatype | TyDefined Ty

-- map (binding positions of) declared type constants to their parameters and appropriate TyInfo
type TyCtxt = M.Map Pos ([Var],TyInfo) 
                                      
-- map extents to their types
type Typings = M.Map Extent Ty

-- also map extents to subtyping constraints introduced there
type Constraints = M.Map Extent [Constraint]

-- map each ctor to its datatype, list of type params, and list of arguments types
type CtorTys = M.Map Pos (Const,[Var],[Ty]) 

-- map each datatype to the list of its ctors
type Ctors = M.Map Pos [Const]

data InferState =
  InferState { tyCtxt :: TyCtxt ,
               tmCtxt :: TmCtxt ,
               tings :: Typings , -- types computed for terms, by the terms' extents

               requireSub :: Constraints , -- constraints the solver needs to satisfy
               requireList :: [EConstraint] ,  

               assumeSub :: Constraints ,  -- constraints the solver should assume
               assumeList :: [Constraint] , 

               subst :: MetaTySubst ,    -- substitution for solved meta-variables

               tyErrs :: [TypeError],  -- type errors

               ctorTys :: CtorTys ,
               ctorsMap :: Ctors ,

               
               metaVarsList :: [MetaVar] , -- the meta-variables and consts we have introduced
               constsList :: [Const],

               -- to present unique type variables to 
               renamingPer :: RenamingPer
  }

initState :: InferState
initState = InferState M.empty M.empty M.empty M.empty [] M.empty [] M.empty [] M.empty M.empty [] [] M.empty 

data TypeError =
   NotCtorInPat Const
 | WrongArityPat Const Int {- expected -} Int {- actual -}
 | WrongCtorsInGamma Extent [Const] {- missing -} [Const] {- extra -}
 | UnsolvedConstraint Extent Constraint
 | TypeErrorsInImport Extent

startTypeError :: Extent -> String
startTypeError (sp,ep) = "(dcs-mode-type-error " ++ elispPos sp ++ " " ++ elispPos ep

endTypeError :: String
endTypeError = "\\n\")"
showTypeError :: TypeError -> String
showTypeError (NotCtorInPat c) =
  startTypeError (extentConst c) ++ "\"  " ++ constStr c
  ++ " is not a constructor, but is used in a pattern"
  ++ endTypeError
showTypeError (WrongArityPat c expected actual) =
  startTypeError (extentConst c) ++ "\"  " ++ constStr c ++ " has arity "
  ++ show expected ++ ", but is used with arity " ++ show actual ++ " in a pattern"
  ++ endTypeError
showTypeError (WrongCtorsInGamma e missing extra) =
  let h name xs = if null xs then "" else name ++ " ctors: " ++ intercalate " " (constStr <$> xs) in
    startTypeError e 
    ++ "\"  " ++ h "missing" missing ++ (if null missing || null extra then "" else "\\n")
    ++ "  " ++ h "extra" extra
    ++ endTypeError
showTypeError (UnsolvedConstraint ext con) =
  startTypeError ext ++ "\"  Unsolved constraint " ++ show con
  ++ endTypeError
showTypeError (TypeErrorsInImport ext) =
  startTypeError ext ++ "\"  Type errors in the imported file or its imports" ++ 
  endTypeError

type InferS b = State InferState b
type Infer a = ScopeMap -> VariancesMap -> a -> InferS ()
type InferInner a b = ScopeMap -> a -> InferS b
type InferTop a = ScopeMap -> VariancesMap -> a -> InferS [TypeError]

infer :: ScopeMap -> VariancesMap -> FileWithDeps -> ([TypeError],InferState)
infer sm vsm f = runState (inferFileWithDeps sm vsm f) initState 

inferFileWithDeps :: InferTop FileWithDeps
inferFileWithDeps sm vsm fd =
  processFileWithDeps (inferSimpleFile sm vsm) TypeErrorsInImport fd

{- process the given simple file, returning any type errors encountered.
   We will clear the type errors out of the InferState at the end of the
   call -}
inferSimpleFile :: InferTop SimpleFile
inferSimpleFile sm vsm f =
  do
    mapM_ (inferStatement sm vsm) f
    st <- get
    let errs = tyErrs st
    clearErrors
    return errs

unfoldDefIf :: ScopeMap -> TyCtxt -> Const -> [Ty] -> Ty
unfoldDefIf sm tyctxt c tyargs =
  let r = TApp(c,tyargs) in
    case M.lookup (varPos $ bindingIf sm c) tyctxt of
      Nothing -> r
      Just (params,TyDatatype) -> r
      Just (params,TyDefined ty) ->
        outerNormalizeTy sm tyctxt $ substTy sm (mkTySubst params tyargs) ty        

{- normalize the given Ty at the top-level only -}
outerNormalizeTy :: ScopeMap -> TyCtxt -> Ty -> Ty
outerNormalizeTy sm tyctxt (TyParens _ ty) = outerNormalizeTy sm tyctxt ty
outerNormalizeTy sm tyctxt (TApp (c,tyargs)) = unfoldDefIf sm tyctxt c tyargs
outerNormalizeTy sm tyctxt ty = ty

addTyParamsTm :: ScopeMap -> [TyParamTm] -> InferS ()
addTyParamsTm sm [] = return ()
addTyParamsTm sm (TyParam _ : params) = addTyParamsTm sm params
addTyParamsTm sm (c@(TyParamLike _ r ty) : params) =
  let ext = extentTyParamTm c in
    assumeConstraint sm ext (Like (tyConst r) ty) >>
    addTyParamsTm sm params

inferStatement :: Infer Statement
inferStatement sm vsm (TyDefSt d) = inferTyDef sm vsm d
inferStatement sm vsm (DataDefSt d) = inferDataDef sm vsm d
inferStatement sm vsm (TmDefSt d) = inferTmDef sm vsm d

inferTyDef :: Infer TyDef
inferTyDef sm vsm (TyDef _ c params ty) =
  withState (\ s -> s { tyCtxt = M.insert (constPos c) (params,TyDefined ty) (tyCtxt s) })
    (return ())


{- ds is an association list mapping mutually recursive datatypes we are currently defining,
   to their type parameters for use in the sig functor -}
funcCtrDef :: ScopeMap -> [(Const,Const)] -> CtrDef -> CtrDef
funcCtrDef sm ds (c',args) = (c', procTy ds <$> args)
  where
    -- replace the datatype(s) with their new type parameters (c to cp)
    procTy :: [(Const,Const)] -> Ty -> Ty
    procTy ds (Arrow ps arr ty1 ty2) = Arrow ps arr (procTy ds ty1) (procTy ds ty2)
    procTy ds (TApp(c,args)) = case lookup (bindingIf sm c) ds of
                                 Nothing -> TApp(c,procTy ds <$> args)
                                 Just x -> tyConst x
    procTy ds (TyParens _ t) = procTy ds t
    procTy ds (TAppMeta _) = undefined -- constructor typings will never contain metavariables

inferDataDef :: Infer DataDef
inferDataDef sm vsm (DataDef _ c params cs) = 
  let cp = ("*", constPos c)
      params' = params ++ [cp]
      d = TApp (c, tyConst <$> params')
  in

    withState
    (\ s ->
       s {
         -- add typings for the ctors, so when we find them in a term outside a pattern, we have their types
         tmCtxt =
         foldr
           (\ (ctor,tys) s -> M.insert (constPos ctor) (TyParam <$> params', arrowTy tys d) s)
           (tmCtxt s)
           (funcCtrDef sm [(c,cp)] <$> cs),
         
         -- add special information for the ctors to ctorTys, to make it easier to type-check gamma-terms
         ctorTys =
           foldr (\ (ctor,tys) s ->
                    M.insert (constPos ctor) (c,params, tys) s)
             (ctorTys s)
             cs,

         -- add a mapping from the datatype to the sorted list of its ctors
         ctorsMap = M.insertWith (++) (constPos c) (sort $ constFromCtrDef <$> cs) (ctorsMap s),

         -- add kinding for the datatype itself
         tyCtxt = M.insert (constPos c) (params,TyDatatype) (tyCtxt s)})
    (return ())

-- this assumes that the given MetaVar is in the domain of the Renaming 
makeTyParamTm :: [Constraint] -> Renaming MetaVar Const -> MetaVar -> TyParamTm
makeTyParamTm cs r m =
  case M.lookup m r of
    Just c ->
      case find likem cs of
        Nothing -> TyParam c
        Just (Like _ ty) -> TyParamLike [] c ty
        Just _ -> undefined
    Nothing -> undefined
  where likem (Like (TAppMeta(m',[])) _) | m == m' = True
        likem _ = False

inferTmDef :: Infer TmDef
inferTmDef sm vsm d@(TmDef _ c params mty t) =
  do
    addTyParamsTm sm params
    ty' <- inferTm sm t
    (needSubst,rty) <- (case mty of
                          Just ty ->
                            do
                              requireConstraint sm (extentTm t) (Subtype ty' ty)
                              return (False,ty)
                          Nothing ->
                            return (True,ty'))
    infst <- get

    -- solve constraints, updating our substitution
    let (assumed,required) = esubtypings infst
    let ((assumed',unsolved),sbst) = runState (solve sm (tyDefs infst) vsm assumed required) (subst infst)

    -- apply the substitution produced by the solver, if there was no annotation for this term def.
    let finalty = if needSubst then substTyMeta sbst rty else rty

    -- map the metavars occuring in the final type to new constants, because these are what we
    -- need when instantiating the type later (with lookupInst)
    let ms = metaVars finalty

    -- now compute renamings for the metavars and constants we introduced, to map them
    -- to uniquely named constants, and vice versa

    -- we sort the metas to pull ones that did not get solved closer to the front.
    -- so they will be given shorter names by renamingFull.
    let isUnsolved m      = M.notMember m (subst infst) 
        metasIntroduced = sortBy (comparing isUnsolved) $ metaVarsList infst
        ren             = renamingFull (constsList infst) ms metasIntroduced

    -- next, we need to convert the MetaVars ms to Consts using rmc (from ren)
    -- in the finalty, and then compute parameters from those.
        (_,rmc,_)       = ren
        finalty'        = substTyMeta (renamingToMetaTySubst tyConst rmc) finalty
        finalparams     = makeTyParamTm assumed' rmc <$> ms

    -- we also compute the updated renamingPer (mapping starting positions of statements to renamings)
        renamingper'    = M.insert (constPos c) ren (renamingPer infst)

    -- add renamed versions of the unsolved constraints as errors
    mapM_ (\ (ext,cn) ->
             addError (UnsolvedConstraint ext
                       (renameConstraintPer sm renamingper' (constPos c) cn)))
      unsolved

    -- add typing for c
    --
    -- we clear the assumeList and requireList at this point, so we don't pile up constraints to solve
    -- across each statement we check.  Similarly, the metaVarsList and constsList
    --
    -- also, we add a FullRenaming to the mapping from starting positions of statements to renaming pairs.
    withState (\ s -> s { tmCtxt = M.insert (constPos c) (params ++ finalparams,finalty') (tmCtxt s) ,
                          subst = sbst,
                          assumeList = [],
                          requireList = [],
                          metaVarsList = [],
                          constsList = [],
                          renamingPer = renamingper'
                        })
      (return ())

{- add the typing of the term at the given extent with the given Ty,
   to our tings structure holding all such typings -}
recordTyping :: Extent -> Ty -> InferS Ty
recordTyping e ty =
  do
    st <- get
    put $ st { tings = M.insert e ty (tings st) }
    return ty

-- record the subtyping constraint at the given extent, as one that needs to hold
requireConstraint :: ScopeMap -> Extent -> Constraint -> InferS ()
requireConstraint sm e sub =
  do
    st <- get
    let sub' = bindingIfConstraint sm sub -- the solver expects binding occurrences of all Consts
    put $ st { requireSub = M.insertWith (++) e [sub'] (requireSub st), requireList = (e,sub') : requireList st }

-- record the subtyping constraint, as one that is assumed to hold
assumeConstraint :: ScopeMap -> Extent -> Constraint -> InferS ()
assumeConstraint sm e sub =
  do
    st <- get
    let sub' = bindingIfConstraint sm sub -- the solver expects binding occurrences of all Consts
    put $ st { assumeSub = M.insertWith (++) e [sub'] (assumeSub st) , assumeList = sub' : assumeList st }

-- return subtypings that are assumed and ones that are to be proven (that order), for the given extent
subtypings :: Extent -> InferState -> ([Constraint],[Constraint])
subtypings ext infst =
  let h m = (case M.lookup ext (m infst) of
                Nothing -> []
                Just stps -> substConstraint (subst infst) <$> reverse stps)
      assumed = h assumeSub
      toprove = h requireSub
  in
    (assumed,toprove)

{- given a binding occurrence, add it with the given type to the tmCtxt,
   and also record its typing -}
addLocal :: Var -> Ty -> InferS Ty
addLocal v ty =
  do
    let ext = extentVar v
    infst <- get
    put $ infst { tmCtxt = M.insert (varPos v) ([],ty) (tmCtxt infst) }
    recordTyping ext ty

recordMeta :: MetaVar -> InferS ()
recordMeta m =
  do
    infst <- get
    put $ infst { metaVarsList = m : metaVarsList infst }

recordConst :: Const -> InferS ()
recordConst c =
  do
    infst <- get
    put $ infst { constsList = c : constsList infst }

{- given a binding occurrence, add it to the tmCtxt with a new meta-var as its type,
   and also record that typing -}
addLocalMeta :: Var -> InferS Ty    
addLocalMeta v =
  do
    let m = varToMeta (extentVar v) v
    recordMeta m
    addLocal v (tyMeta m)

-- lookup the given variable in the TmCtxt, and instantiate its parameters with fresh meta-vars.
-- Some of those meta-vars might be likeness-constrained, so return those constraints
lookupInst :: ScopeMap -> TmCtxt -> Var -> InferS (Ty,[Constraint])
lookupInst sm tmctxt v = 
  case M.lookup (varPos (bindingIf sm v)) tmctxt of
    Nothing ->
      {- undefined symbol; we will report this error via UndefinedSyms.hs.
         Here, we just need to keep type inference going, so add a meta-variable
         for the var. -}
      return (tyMeta ("Undefined",extentVar v),[])
    Just (params,ty) ->
     {- we have a type for this term variable.  Instantiate the type's parameters
        with fresh meta-variables. Also, extract any likeness constraints and
        apply that same instantiation to their types -}
      let ext      = extentVar v
          s        = paramMetaSubstTm ext params
          (ms, ls) = unzip $
                     map (\ (TyParamLike _ v ty) ->
                            let m = varToMeta ext v in
                              (m, Like (tyMeta m) (substTy sm s ty))) $
                     filter isTyParamLike params
      in
        do
          mapM_ recordMeta ms
          return (substTy sm s ty, ls)

-- infer a type for a term
inferTm :: InferInner Tm Ty
inferTm sm (Var v) =
  do
    tmctxt  <- tmCtxt <$> get
    (ty,ls) <- lookupInst sm tmctxt v
    let ext = extentVar v
    mapM_ (requireConstraint sm ext) ls
    recordTyping ext ty
inferTm sm t@(Lam _ vs body) =
  do
    -- add typings for the variables vs
    tys <- mapM addLocalMeta vs
    -- then infer types for the body
    ty <- inferTm sm body
    recordTyping (extentTm t) $ arrowTy tys ty
inferTm sm t@(Parens _ t') =
  do
    let ext = extentTm t
    ty <- inferTm sm t'

    recordTyping ext ty

inferTm sm t@(App t1 t2) =
  do
    let ext = extentTm t
    tyctxt <- tyCtxt <$> get
    ty1 <- outerNormalizeTy sm tyctxt <$> inferTm sm t1
    ty2 <- inferTm sm t2
    case ty1 of
      Arrow _ ArrowPlain ty1a ty1b ->
        do
          requireConstraint sm ext (Subtype ty2 ty1a)
          recordTyping ext ty1b
      _ ->
        do 
          let m = ("ret", ext)
              r = tyMeta m
          recordMeta m
          requireConstraint sm ext (Subtype ty1 (Arrow [] ArrowPlain ty2 r))
          recordTyping ext r
inferTm sm t@(Sigma _ bs body) =
  do
    mapM_ (inferSigmaBinding sm) bs
    inferTm sm body
inferTm sm t@(Omega _ f xs carrier body) =
  do
    let ext = extentTm t
        m  = ("F", ext)
        vF = tyMeta m
        c1 = ("P", startingPosTm t)
        cP = tyConst c1
        c2 = ("R", startingPosTm t)
        cR = tyConst c2
        fr = TAppMeta(("F",ext),[cR])
    recordMeta m
    recordConst c1
    recordConst c2
    addLocal f (Arrow [] ArrowPlain cR (tapp (TApp carrier) [cR,cR]))
    addLocal xs fr
    ty <- inferTm sm body

    -- R ~ F
    assumeConstraint sm ext (Like cR vF)

    -- F R <: P
    assumeConstraint sm ext (Subtype fr cP)

    -- R <: P
    assumeConstraint sm ext (Subtype cR cP) -- follows transitively from first two assumptions

    -- ty <: Carrier R P
    requireConstraint sm ext (Subtype ty (tapp (TApp carrier) [cR,cP]))

    -- F => Carrier
    recordTyping ext (Arrow [] ArrowFat vF (TApp carrier))
inferTm sm t@(Gamma _ scrut cs) =
  do
    let ext = extentTm t
        m = ("Case", ext)
        r = tyMeta m
    recordMeta m
    ty <- inferTm sm scrut 
    q <- mapM (inferCase sm) cs 

    -- helper function to add subtyping constraints for all cases that we could check
    let subtypeCase :: Maybe (Case,Ty,Ty) -> InferS ()
        subtypeCase Nothing = return ()
        subtypeCase (Just (caase,patty,bodyty)) =
          let extc = extentCase caase in
            requireConstraint sm extc (Subtype ty patty) >>
            requireConstraint sm extc (Subtype bodyty r)            

    mapM_ subtypeCase q

    -- check that we have all the constructors for a particular datatype, in the patterns
    let patctrs = sort $ (bindingIf sm . constFromCase) <$> cs
        finish = recordTyping ext r
    ctorsmap <- ctorsMap <$> get
    ctortys <- ctorTys <$> get    
    case patctrs of
      [] -> finish -- fixme: state that type of scrutinee is subtype of special empty (bottom) type
      (c:cs) ->
        case M.lookup (constPos c) ctortys of
          Nothing -> finish
          Just (d,_,_) ->
            case M.lookup (constPos d) ctorsmap of
              Nothing -> finish
              Just ctrs ->
                do
                  let missing = ctrs \\ patctrs
                      extra = patctrs \\ ctrs
                  unless (null missing && null extra)
                    (addError (WrongCtorsInGamma (fst ext,fst ext) missing extra))
                  recordTyping ext r

inferSigmaBinding :: InferInner SigmaBinding Ty
inferSigmaBinding sm (SigmaBinding _ x t) =
  do
    ty <- inferTm sm t
    addLocal x ty

addError :: TypeError -> InferS ()
addError e =
  do
    st <- get
    put $ st { tyErrs = e : tyErrs st }

clearErrors :: InferS ()
clearErrors =
  do
    st <- get
    put $ st { tyErrs = [] }

{- unless we cannot type check the pattern,
   return the case itself, the type of the pattern, and the type of the body -}
inferCase :: InferInner Case (Maybe (Case,Ty,Ty))
inferCase sm caase@(Case p@(Pat c vs) body) =
  do
    ctorTys <- ctorTys <$> get
    let notctor = (addError (NotCtorInPat c) >>
                   return Nothing)
    case M.lookup (varPos c) sm of
      Nothing -> notctor
      Just c' ->
        case M.lookup (varPos c') ctorTys of
          Nothing -> notctor
          Just (d,params,cargtys) -> -- cargtys are the types of the arguments expected by ctor c
            do
              let ext = extentConst c
                  expected = length cargtys
                  actual = length vs 
                  psubst = paramMetaSubst ext params
                  m      = ("*", ext)
                  funcm = tyMeta m

                  -- replace params with new meta-vars in the cargtys
                  -- also, use abstractTApp to replace uses or applications of d with funcm

                  cargtys' = (abstractTApp sm d funcm . substTy sm psubst) <$> cargtys

                  {- this is the list of those new meta-vars (not conveniently returned by metaParamsTy),
                     with also the functorial parameter added at the end -}
                  mparams = varToMeta ext <$> params
                  params' = (tyMeta <$> mparams) ++ [funcm] 

              mapM recordMeta mparams
              recordMeta m

              -- add types for as many of the pattern variables as possible 
              zipWithM_ addLocal vs cargtys'

              -- record error if the number of pattern variables is wrong
              when (expected /= actual) 
                (addError (WrongArityPat c expected actual))

              ty <- inferTm sm body

              let pty = TApp (d,params')

              recordTyping (extentPat p) pty

              return $ Just (caase, pty, ty)

{- abstract away application of the given Const, to the given Ty.
   This is used above to abstract away uses of D in the argument types for a constructor for D -}
abstractTApp :: ScopeMap -> Const -> Ty -> Ty -> Ty
abstractTApp sm d r (TApp(d',tys)) = if d == (bindingIf sm d') then r else TApp(d',abstractTApp sm d r <$> tys)
abstractTApp sm d r (Arrow _ arr ty1 ty2) = Arrow [] arr (abstractTApp sm d r ty1) (abstractTApp sm d r ty2)
abstractTApp sm d r (TAppMeta(m,tys)) = TAppMeta (m, (abstractTApp sm d r <$> tys))
abstractTApp sm d r (TyParens _ ty) = abstractTApp sm d r ty

showRenamedTyping :: ScopeMap -> RenamingPer -> Pos -> Const -> [TyParamTm] -> Ty -> String
showRenamedTyping sm ren pos c params ty =
  constStr c ++
  showListBy showTyParamTm (renameTyParamTmPer sm ren pos <$> params) ++
  " : " ++
  showTy (renameTyPer sm ren pos ty)

showRenamedConstraint :: ScopeMap -> RenamingPer -> Pos -> Constraint -> String
showRenamedConstraint sm ren pos c = showConstraint $ renameConstraintPer sm ren pos c

elispTyping :: ScopeMap -> RenamingPer -> Pos -> Const -> [TyParamTm] -> Ty -> String
elispTyping sm ren pos c params ty =
  "(dcs-mode-inspect \"" ++ showRenamedTyping sm ren pos c params ty ++ "\")"

{- generate elisp (as a String) to show the inspect buffer for the
   given extent. -}
inspectTy :: ScopeMap -> InferState -> Extent -> String
inspectTy sm infst ext@(sp,ep) =
  let sbst = subst infst
      (assumed,toprove) = subtypings ext infst
      ren = renamingPer infst

      r ss = concat ["\\n  " ++ showRenamedConstraint sm ren sp s | s <- ss]
      
      h str = ("(dcs-mode-inspect \"" ++ str
               ++ r assumed
               ++ "\\n  --------------------------------------------------"
               ++ r toprove
               ++ "\\n\")")
  in
    case M.lookup ext (tings infst) of
      Nothing ->
        if null assumed && null toprove then
          "(message \"No type info available\")"
        else
          h ""
      Just ty ->
        let ty' = substTyMeta sbst ty in
          h (showTy (renameTyPer sm ren sp ty') ++ "\\n")

--------------------------------------------------

-- return the assumed Constraints and required EConstraints
esubtypings :: InferState -> ([Constraint],[EConstraint])
esubtypings infst =
  (assumeList infst, requireList infst)

tyDefs :: InferState -> TyDefs
tyDefs infst =
  M.mapMaybe (\ (params,tyinfo) ->
                case tyinfo of
                  TyDatatype -> Nothing
                  TyDefined ty -> Just (params,ty))
  (tyCtxt infst)