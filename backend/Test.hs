module Test where

import Pos
import Arities
import Parser
import Lexer
import Syntax
import ScopeMap
import Variances
import Infer
import SubstTy
import Data.List
import Data.Text(Text,unpack,pack)
import qualified Data.Map as M
import Control.Monad.Reader
import Control.Monad.State.Lazy
import Subtyping

test :: IO ([Subtyping],[ESubtyping])
test =
  do 
    contents <- readFile "../examples/test.dcs"

    case parseTxt (pack contents) of 
      Right file ->
        do

          let sm = scopeMap file 

          let (varerrs,(vsm,vm)) = variances sm file

          let action =
                do
                  inferStatement sm vsm (file !! 0)
                  let (TmDefSt (TmDef _ c params mty t)) = file !! 1

                  ty <- inferTm sm t
                  infst <- get
                  let (assumed,required) = esubtypings infst
                  return (assumed,required)

          return $ evalState action initState
          


