Definition Carrier : Type := Set -> Set -> Set.

Definition KAlg  : Type := Carrier -> Set.

Definition CastAlg(alg1 alg2 : KAlg) :=
  forall (X : Carrier), alg1 X -> alg2 X.

