;; -*- lexical-binding: t; -*-
(defconst dcs-command-delimiter "$"
  ;; FIXME: AFAICT this is also the delimiter used when receiving
  ;; results, right?
  "The delimiting character for commands sent to the backend.
\(should agree with similar constant in Consts.hs)")

(provide 'dcs-consts)
